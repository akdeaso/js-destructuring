//Destructuring Array
const listSiswa = ["Andi", "Budi", "Cindy", "Dini", "Eva"];
const [siswa1, siswa2, siswa3, ...siswaLainnya] = listSiswa;

console.log(siswa1, siswa2, siswa3, siswaLainnya);

//Destructuring Object
const produk = {
  nama: "Kemeja",
  harga: 200000,
  stok: 50,
  brand: "Nakama",
  warna: "Misty",
};

const { nama, harga, ...detailProduk } = produk;
console.log(nama, harga, detailProduk);

//Destructuring dalam Function Parameter
const dataSiswa = {
  nama: "Andi",
  kelas: "XII-IPA-7",
  nilai: [80, 85, 90, 78, 95],
};

const dataProduct = {
  nama: "Sepatu",
  harga: 500000,
  brand: "Abidas",
  ukuran: "43",
};

const calculateAverage = ([nilai1, nilai2, nilai3, nilai4, nilai5]) => {
  const average = (nilai1 + nilai2 + nilai3 + nilai4 + nilai5) / 5;
  console.log(`Rata-rata nilai siswa: ${average}`);
};

const printProductDetails = ({ nama, harga, ...details }) => {
  console.log(`Product: ${nama}, Harga: ${harga}, Detail Lain:`, details);
};

calculateAverage(dataSiswa.nilai);
printProductDetails(dataProduct);
